# ics-ans-manage-facts

Ansible playbooks to manage facts (gather and clear them):

- gather-facts.yml
- clear-facts.yml

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
